irrBullet - Bullet Physics Wrapper for Irrlicht Engine
======================================================

Details
-------
This is a fork of irrBullet, which can be found <a href="https://irrbullet.svn.sourceforge.net/">here</a>. 

It is an updated version of irrBullet which works with the most up to date versions of Irrlicht and Bullet.

What's new in 2.0
-------------------
* Performance improvement (refactored loops, replaced Irrlicht's containers with STL, add move semantics etc)
* Several bug fixes
* Support for latest versions of Bullet and Irrlicht
* ICapsuleShape and ICylinderShape added
* IRigidBody, irrBulletWorld etc are feature complete (all methods have been wrapped)
* Strong C++14 support - dropped support for non-C++14 compilers
* irrBullet is forked - now maintained by Brigham Keys and Nicolas Ortega

Installation
------------
(Note: Only Linux supported at the moment)

This is yet to be documented

License
-------
irrBullet is licensed as free (libre) software and is licensed with the [GNU GPLv3](LICENSE). However, in the future we will make the option available to purchase the rights to use the library under the GNU LGPLv3.
