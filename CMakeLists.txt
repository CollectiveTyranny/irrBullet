cmake_minimum_required(VERSION 2.8)

set(IRRBULLET_SOURCE_DIR ${CMAKE_SOURCE_DIR}/src/)
set(IRRBULLET_BINARY_DIR ${CMAKE_SOURCE_DIR}/libs/)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake_modules/)

option(IS_SUBMODULE "Build as a submodule using static dependencies in previous directory." FALSE)

include(irrBullet.cmake)

#file(COPY ${CMAKE_SOURCE_DIR}/media/ DESTINATION media/)
#add_subdirectory(examples/)
