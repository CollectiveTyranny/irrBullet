cmake_minimum_required(VERSION 2.8)
project(irrBullet)

set(IRRBULLET_VERSION 2.0)

  find_path(BULLET_INCLUDE_DIR
    NAMES
      bullet3-2.83.7/src/btBulletCollisionCommon.h
    PATHS
      .
  )

set(BULLET_INCLUDE_DIR "." CACHE DIRECTORY "include directory for Bullet Physics")

set(IRRLICHT_BIN "." CACHE FILEPATH "Binary from Irrlicht that you would like to link")
set(BULLET_COLLISION_BIN "." CACHE FILEPATH "Binary for Collision module for Bullet")
set(BULLET_DYNAMICS_BIN "." CACHE FILEPATH "Binary for Dynamics module for Bullet")
set(BULLET_INVERSEDYNAMICS_BIN "." CACHE FILEPATH "Binary for Inverse Dynamics module for Bullet")
set(BULLET_SOFTBODY_BIN "." CACHE FILEPATH "Binary for Soft Body module for Bullet")
set(BULLET_LINMATH_BIN "." CACHE FILEPATH "Binary for the Linear Math module of Bullet")

set(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS TRUE)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

set(LIBRARY_OUTPUT_PATH libs/)

if(WIN32)
set(CMAKE_CXX_FLAGS " -std=gnu++14")
else()
set(CMAKE_CXX_FLAGS " -std=c++14")
endif()
set(CMAKE_CXX_FLAGS_DEBUG " -std=c++14 -pedantic-errors -g -Wall -O0")
set(CMAKE_CXX_FLAGS_RELEASE " -std=c++14 -O3")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO " -std=c++14 -pedantic-errors -g -Wall -O3")
set(CMAKE_CXX_FLAGS_MINSIZEREL " -std=c++14 -Os")

set(SRCS
    src/irrBulletBoxShape.cpp
    src/irrBulletCollisionObjectAffectorDelete.cpp
    src/irrBulletCylinderShape.cpp
    src/irrBulletRayCastVehicle.cpp
    src/irrBulletBvhTriangleMeshShape.cpp
    src/irrBulletCollisionObject.cpp
    src/irrBulletGImpactMeshShape.cpp
    src/irrBulletRigidBody.cpp
    src/irrBulletCapsuleShape.cpp
    src/irrBulletCollisionShape.cpp
    src/irrBulletKinematicCharacterController.cpp
    src/irrBulletSoftBody.cpp
    src/irrBulletCollisionCallBackInformation.cpp
    src/irrBulletcommon.cpp
    src/irrBulletLiquidBody.cpp
    src/irrBulletSphereShape.cpp
    src/irrBulletCollisionObjectAffectorAttract.cpp
    src/irrBulletConvexHullShape.cpp
    src/irrBulletMotionState.cpp
    src/irrBulletTriangleMeshShape.cpp
    src/irrBulletCollisionObjectAffector.cpp
    src/irrBullet.cpp
    src/irrBulletPhysicsDebug.cpp
    src/irrBulletWorld.cpp
)

set(HDRS
    src/irrBulletBoxShape.h
    src/irrBulletCollisionObject.h
    src/irrBullet.h
    src/irrBulletSoftBody.h
    src/irrBulletBvhTriangleMeshShape.h
    src/irrBulletCollisionShape.h
    src/irrBulletKinematicCharacterController.h
    src/irrBulletSphereShape.h
    src/irrBulletCapsuleShape.h
    src/irrBulletCommon.h
    src/irrBulletLiquidBody.h
    src/irrBulletTriangleMeshShape.h
    src/irrBulletCollisionCallBackInformation.h
    src/irrBulletCompileConfig.h
    src/irrBulletMotionState.h
    src/irrBulletWorld.h
    src/irrBulletCollisionObjectAffectorAttract.h
    src/irrBulletConvexHullShape.h
    src/irrBulletPhysicsDebug.h
    src/irrBulletCollisionObjectAffectorDelete.h
    src/irrBulletCylinderShape.h
    src/irrBulletRayCastVehicle.h
    src/irrBulletCollisionObjectAffector.h
    src/irrBulletGImpactMeshShape.h
    src/irrBulletRigidBody.h
)

find_package(Irrlicht REQUIRED)

if(UNIX AND NOT APPLE)
    if(NOT IS_SUBMODULE)
    endif()

    message("GNU/Linux detected")
    if(NOT IS_SUBMODULE)
        include_directories( SYSTEM
            ${IRRLICHT_INCLUDE_DIR}
            ${BULLET_INCLUDE_DIRS}
            ${CMAKE_SOURCE_DIR}/src/)
    endif()
else()
    message("External Lib Build")
    include_directories( SYSTEM
        ${OPENGL_INCLUDE_DIR}
        "${CMAKE_SOURCE_DIR}/src")
endif()

add_library(${CMAKE_PROJECT_NAME} STATIC ${SRCS} ${HDRS})

set_target_properties(${CMAKE_PROJECT_NAME}
    PROPERTIES
    OUTPUT_NAME "irrBullet"
    DEBUG_POSTFIX "-dbg"
    RELWITHDEBINFO_POSTFIX "-dbg"
)

if(UNIX AND NOT APPLE)
    if(NOT IS_SUBMODULE)
        target_link_libraries(${CMAKE_PROJECT_NAME}
            ${IRRLICHT_LIBRARY}
            ${BULLET_LIBRARIES})
    endif()
else()

    include_directories(
    ${IRRLICHT_INCLUDE_DIR}
    ${BULLET_INCLUDE_DIR}
    )

    target_link_libraries(${CMAKE_PROJECT_NAME}
    IRRLICHT_BIN
    BULLET_COLLISION_BIN
    BULLET_DYNAMICS_BIN
    BULLET_INVERSEDYNAMICS_BIN
    BULLET_SOFTBODY_BIN
    BULLET_LINMATH_BIN
    )
endif()
